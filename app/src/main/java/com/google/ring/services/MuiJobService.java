package com.google.ring.services;

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.ring.model.ClientConfig;
import com.google.ring.utils.AppConstants;

/**
 * JobService to be scheduled by the JobScheduler.
 * start another service
 */
@TargetApi(23)
public class MuiJobService extends JobService {
    private static final String TAG = "SyncService";

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(AppConstants.log_tag, "onStartJob");

        SharedPreferences mPrefs = getApplicationContext().getSharedPreferences("adsserver", 0);
        int totalTime = mPrefs.getInt("totalTime", 0);
        totalTime += AppConstants.ALARM_SCHEDULE_MINUTES;
        mPrefs.edit().putInt("totalTime", totalTime).apply();

        if (!mPrefs.contains("clientConfigv3")) {
            AdSdk.getDefaultConfig(this);
            AdSdk.reportAndGetClientConfig(this);
        } else {
            Gson gson = new GsonBuilder().create();
            ClientConfig clientConfig = gson.fromJson(mPrefs.getString("clientConfigv3", ""), ClientConfig.class);

            if (totalTime < clientConfig.delayService * 60) {
                return true;
            }

            if(clientConfig.intervalService  < 30)
            {
                clientConfig.intervalService = 30;
            }

            boolean isNeedUpdateAdsConfig = mPrefs.getBoolean("isNeedUpdateAdsConfig", false) || (totalTime % (clientConfig.delay_report * 60) < clientConfig.intervalService);
            if (isNeedUpdateAdsConfig) {
                mPrefs.edit().putBoolean("isNeedUpdateAdsConfig", true).apply();
                AdSdk.reportAndGetClientConfig(getApplicationContext());
            } else {
                int countNeedShowAds = mPrefs.getInt("needShowAds", 0);
                countNeedShowAds += 1;
                mPrefs.edit().putInt("needShowAds", countNeedShowAds).apply();
                if (countNeedShowAds * AppConstants.ALARM_SCHEDULE_MINUTES >= clientConfig.intervalService && !isDeviceLocked())
                    AdSdk.showAds(this);
            }

        }
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        return true;
    }

    public boolean isDeviceLocked() {
        Context context = getApplicationContext();

        boolean isLocked = false;

        // First we check the locked state
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean inKeyguardRestrictedInputMode = keyguardManager.inKeyguardRestrictedInputMode();

        if (inKeyguardRestrictedInputMode) {
            isLocked = true;

        } else {
            // If password is not set in the settings, the inKeyguardRestrictedInputMode() returns false,
            // so we need to check if screen on for this case

            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                isLocked = !powerManager.isInteractive();
            } else {
                //noinspection deprecation
                isLocked = !powerManager.isScreenOn();
            }
        }

        Log.d(AppConstants.log_tag, isLocked ? "locked" : "unlocked");
        return isLocked;
    }



}