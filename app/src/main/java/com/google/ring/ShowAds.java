package com.google.ring;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;


public class ShowAds extends AppCompatActivity {
    private static ShowAds instance;
    public static ShowAds getInstance() {
        return instance;
    }
    private int countResume = 0;
    private ProgressDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try
        {
            dialogLoading = new ProgressDialog(this); // this = YourActivity
            dialogLoading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogLoading.setIndeterminate(true);
            dialogLoading.setCanceledOnTouchOutside(false);
            dialogLoading.show();
        }
        catch (Exception e){}

        if (instance == null)
            instance = this;
    }

    @Override
    public void onResume(){
        super.onResume();
        countResume++;

        try {
            if(countResume >= 2)
            {
                if (Build.VERSION.SDK_INT < 21) {
                    finishAffinity();
                } else {
                    finishAndRemoveTask();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ( dialogLoading!=null)
        {
            dialogLoading.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }

}
