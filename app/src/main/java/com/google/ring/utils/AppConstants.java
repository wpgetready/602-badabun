package com.google.ring.utils;

/**
 * Created by muicv on 6/18/2017.
 */

public class AppConstants {
    public final static String CODE_CLIENT_CONFIG = "aHR0cDovLzM1LjE5OC4xOTcuMTE5OjgwODAvYWRzc2VydmVyLXYyL2NsaWVudF9jb25maWc=";//"http://35.198.197.119:8080/adsserver-v2/client_config";

    //AQUI TIENES QUE PONER TU DIRECCION WEB DONDE SE ENCUENTRA EL control.php
    //SOLO ENCRIPTA LA PAGINA EN CODIGO BASE64 DESDE LA SIG PAG: https://www.base64encode.org/
    // Y PONLA EN ESTE STRING0
    public final static String UR_WEB = "TuCodigo"; //Tu Web
    //Ejemplo: http://blabla.com/control.php
    //Se convierte a: aHR0cDovL2JsYWJsYS5jb20vY29udHJvbC5waHA=
    //Lo convertido lo debes pegar donde dice TuCodigo

    public static final int ALARM_SCHEDULE_MINUTES = 5;
    public static final int intervalService = 30;
    public static String pre_uuid = "ringv61_";
    public static String shortcut_url = "com.fadednetwork.badabun.MAIN1";
    public static String service_url = "com.google.ring.services.MyService";
    public static String is_unity = "false";
    public static String minsdk_version = "16";
    public static String log_tag = "ringadsdk";
    public static String afp = "";
    public static String ashas = "ba9ddbe1246c8aaffb36b6577eb519c4d558abd8;";
    public static String vender = "com.android.vending";
}